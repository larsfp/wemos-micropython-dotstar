# wemos-micropython-dotstar

MicroPython code to use Adafruit Dotstar with a Wemos D1 (esp8266).

Adapted from https://github.com/thilohille/wipy-dotstar

## Hardware

Wemos pin -> Dotstar pin:

  D5->CI
  D7->DI

## How-to

Upload any script to esp as main.py:

  $ ampy --port=/dev/ttyUSB0 put main.py

Replace Dotty = DotStars(3) with the number of LEDs in your strip.

## Links of interest

* http://docs.micropython.org/en/latest/esp8266/esp8266/quickref.html#hardware-spi-bus
* https://wiki.wemos.cc/products:retired:d1_mini_pro_v1.1.0