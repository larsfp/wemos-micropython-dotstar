from machine import SPI
import time

class DotStars:

    def __init__(self, leds):
        self.ledcount = leds
        self.buffersize = self.ledcount * 4
        self.buffer = bytearray(self.ledcount * 4)
        self.emptybuffer = bytearray(self.ledcount * 4)
        for i in range(0, self.buffersize, 4):
            self.emptybuffer[i] = 0xff
            self.emptybuffer[i + 1] = 0x0
            self.emptybuffer[i + 2] = 0x0
            self.emptybuffer[i + 3] = 0x0   
        self.startframe = bytes([0x00, 0x00, 0x00, 0x00])
        self.endframe   = bytes([0xff, 0xff, 0xff, 0xff])
        #self.spi = SPI(0, mode=SPI.MASTER, baudrate=8000000, polarity=0, phase=0,bits=8, firstbit=SPI.MSB)
        self.spi = SPI(1, baudrate=8000000, polarity=0, phase=0)
        self.clearleds()
       
    #init empty self.buffer
    def clearleds(self):
        self.buffer = self.emptybuffer[:]

    def setled(self, led, red=0, green=0, blue=0, bri=0x1f):
        if (led > self.ledcount):
            led=led % self.ledcount
        
        if (led < 0):
            led = self.ledcount + led
        
        frameheader = (0x07 << 5) | bri
        offset = led * 4
        self.buffer[offset] = frameheader
        self.buffer[offset + 1] = blue
        self.buffer[offset + 2] = green
        self.buffer[offset + 3] = red

    def send(self):
        self.spi.write(self.startframe + self.buffer + self.endframe)
        #self.spi.write(self.startframe + self.buffer)
 
if __name__ == '__main__':
    import os
    Dotty = DotStars(10)
    Dotty.clearleds()
    Dotty.send()
    brightness = 1

    print("Setting all to green")
    r=0
    g=250
    b=0
    for i in range(0,Dotty.ledcount):
        Dotty.setled(led=i, red=r, green=g, blue=b, bri=brightness)

    Dotty.send()
    