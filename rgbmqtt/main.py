# -*- coding: utf-8 -*-
# Copyright (C) 2017  Costas Tyfoxylos
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from umqtt.robust import MQTTClient
#import ujson
import esp
import time
import machine
from machine import SPI
#import ubinascii # Convert bytes to hex
from ustruct import unpack

led_count = 0 # Change in configuration.json

state = "unknown"
brightness = 0 # max is 0x1f / 31
c = None
r=0
g=0
b=0

class DotStars:

    def __init__(self, leds):
        self.ledcount = leds
        self.buffersize = self.ledcount * 4
        self.buffer = bytearray(self.ledcount * 4)
        self.emptybuffer = bytearray(self.ledcount * 4)
        for i in range(0, self.buffersize, 4):
            self.emptybuffer[i] = 0xff
            self.emptybuffer[i + 1] = 0x0
            self.emptybuffer[i + 2] = 0x0
            self.emptybuffer[i + 3] = 0x0   
        self.startframe = bytes([0x00, 0x00, 0x00, 0x00])
        self.endframe   = bytes([0xff, 0xff, 0xff, 0xff])
        self.spi = SPI(1, baudrate=8000000, polarity=0, phase=0)
        self.clearleds()
       
    #init empty self.buffer
    def clearleds(self):
        self.buffer = self.emptybuffer[:]

    def setled(self, led, red=0, green=0, blue=0, bri=0x1f):
        if (led > self.ledcount):
            led=led % self.ledcount
        
        if (led < 0):
            led = self.ledcount + led
        
        frameheader = (0x07 << 5) | bri
        offset = led * 4
        self.buffer[offset] = frameheader
        self.buffer[offset + 1] = blue
        self.buffer[offset + 2] = green
        self.buffer[offset + 3] = red

    def send(self):
        self.spi.write(self.startframe + self.buffer + self.endframe)

def sub_cb(topic, msg):
    global state, c, brightness, r, g, b
    print(topic, msg)
    Dotty = DotStars(led_count)

    if '/switch' in topic:
        if msg == b"ON":
            print("Turning on all leds")
            state = "ON"
            r=250
            g=250
            b=250
            brightness=0x1f
        else:
            print("Turning off all leds")
            state = "OFF"
            brightness=0

        for i in range(0,Dotty.ledcount):
            Dotty.setled(led=i, red=r, green=g, blue=b, bri=brightness)
        Dotty.send()

    elif msg == b"green":
        print("Setting all to green")
        state = "green"
        r=0
        g=250
        b=0
        for i in range(0,Dotty.ledcount):
            Dotty.setled(led=i, red=r, green=g, blue=b, bri=brightness)
        Dotty.send()

    if '/brightness/set' in topic:
        if 0 == len(msg):
            print("Error: msg was %s" % msg)
            return

        msg = int(msg.format(1))
        print("Setting brightness to %s" % msg)
        brightness = msg
        
        for i in range(0,Dotty.ledcount):
            Dotty.setled(led=i, red=r, green=g, blue=b, bri=brightness)
        Dotty.send()

    if '/rgb/set' in topic:
        print("Setting rgb to %s" % msg)
        r,g,b = msg.split(',')
        for i in range(0,Dotty.ledcount):
            Dotty.setled(led=i, red=r, green=g, blue=b, bri=brightness)
        Dotty.send()

    # Status is always sent
    c.publish(topic + '/status', state)
    c.publish(topic + '/brightness/status', '%s' % brightness)
    c.publish(topic + '/rgb/status', '%s,%s,%s' % (r, g, b))

def main():
    global led_count, c

    client_id = configuration.get('client_id')
    topic = client_id + configuration.get('topic')
    submit_interval = int(configuration.get('submit_interval'))
    exception_timeout = int(configuration.get('exception_reset_timeout'))
    mqtt_server_ip = configuration.get('mqtt_server_ip')
    mqtt_user=configuration.get('mqtt_user')
    mqtt_password=configuration.get('mqtt_password')
    led_count = int(configuration.get('led_count'))

    # commands: status

    try:
        network_setup(configuration)

        c = MQTTClient(
            client_id,
            mqtt_server_ip,
            user=mqtt_user,
            password=mqtt_password)

        # Subscribed messages will be delivered to this callback
        c.set_callback(sub_cb)
        c.connect()
        c.subscribe(topic + '/switch')
        c.subscribe(topic + '/status')
        c.subscribe(topic + '/rgb/set')
        c.subscribe(topic + '/brightness/set')
        
        print("Connected to %s, subscribed to topic %s." % (mqtt_server_ip, topic))

        # Status
        c.publish(topic + '/status', state)
        c.publish(topic + '/brightness/status', '%s' % brightness)
        c.publish(topic + '/rgb/status', '%s,%s,%s' % (r, g, b))

        print("Waiting for command. Try to send i.e. <%s/switch ON> via mqtt" % topic )
        while 1:
            #micropython.mem_info()
            c.wait_msg()

    except Exception as e:
        print(('Caught exception, {}'
            'resetting in {} seconds...').format(e, exception_timeout))
        time.sleep(exception_timeout)
        machine.reset()

if __name__ == '__main__':
    main()
