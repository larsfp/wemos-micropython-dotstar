# MQTT Light

## Goal

To make a light strip compatible with https://www.home-assistant.io/components/light.mqtt/ using MicroPython, Dotstar, Wemos D1.

## Links

https://github.com/micropython/micropython-lib/tree/master/umqtt.simple

## Example configuration.yaml entry

```yaml
light:
  - platform: mqtt
    name: "Office Light RGB"
    state_topic: "office/rgb/light/status"
    command_topic: "office/rgb/light/switch"
    brightness_state_topic: "office/rgb/brightness/status"
    brightness_command_topic: "office/rgb/brightness/set"
    rgb_state_topic: "office/rgb/rgb/status"
    rgb_command_topic: "office/rgb/rgb/set"
    #state_value_template: "{{ value_json.state }}"
    #brightness_value_template: "{{ value_json.brightness }}"
    #rgb_value_template: "{{ value_json.rgb | join(',') }}"
    qos: 0
    payload_on: "ON"
    payload_off: "OFF"
    optimistic: false
```
